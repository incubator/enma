package org.chorem.enma.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Money no extends EnmaEntity, this is not an entity
 */
@Getter
@Setter
@ToString(callSuper = true)
public class Money {
    private long amount;
    private String currencyCode;
    private long fractionDigits;

    public Money(Money m) {
        this.amount = m.amount;
        this.currencyCode = m.currencyCode;
        this.fractionDigits = m.fractionDigits;
    }

    public Money add(Money m) {
        Money result = new Money(this);
        result.amount += m.amount;
        return result;
    }
}
