package org.chorem.enma.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString(callSuper = true)
public class Category extends EnmaEntity {
    private List<Category> childs = new ArrayList<>();
    private String name;
}
