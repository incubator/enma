package org.chorem.enma.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public abstract class EnmaEntity {
    private String uid;
    private List<Editor> editors = new ArrayList<>();
    private List<Note> notes = new ArrayList<>();
    private List<Document> documents = new ArrayList<>();
}
