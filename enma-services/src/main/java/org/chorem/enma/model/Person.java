package org.chorem.enma.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class Person extends Actor {
    private String title; // Dr., M., Mme., ...
    private String firstName;
    private String lastName;
    private String diploma;
}
