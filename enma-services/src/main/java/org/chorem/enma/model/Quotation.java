package org.chorem.enma.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString(callSuper = true)
public class Quotation extends Period {
    private String reference;
    private String description;
    private List<Task> tasks = new ArrayList<>();

    private List<StatusChange> status = new ArrayList<>();

    private Project project;
    private Employee supplier;
    private Employee customer;

    private double estimatedDays;

    private int warrantyPeriod; // nombre de jour de garantie
    private int rsvPeriod; // nombre de jour de vsr

    private Money amount;
    private double vat;

    private int conversionHope;

    public static class StatusChange {
        Date date;
        String comment;
        Status status = Status.draft;
    }

    public enum Status {
        draft, canceled, sent, rejected, accepted, started, delivered, rsv, warranty, closed
    }
}
