package org.chorem.enma.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString(callSuper = true)
public class Period extends EnmaEntity {
    private Date beginDate;
    private Date endDate;

    public long getDuration() {
        return endDate.getTime() - beginDate.getTime();
    }
}
