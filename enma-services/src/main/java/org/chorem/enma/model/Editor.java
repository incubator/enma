package org.chorem.enma.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * Editor no extends EnmaEntity, this is not an entity
 */
@Getter
@Setter
public class Editor {
    private Date date;
    private User user;

    @Override
    public String toString() {
        return "user: " + (user == null ? null : user.getLogin()) + " date: " + date;
    }
}
