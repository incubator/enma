package org.chorem.enma.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class ConsumedTime extends Period {
    private Actor worker;
}
