package org.chorem.enma.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString(callSuper = true)
public class Employee extends Actor {
    private Person person;
    private Company company;
    private Date hiringDate;
    private String situation; // le poste
    private Money annualSalary;
    private Money annualCost;
    private int annualWorkingTime;
}
