package org.chorem.enma.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString(callSuper = true)
public class FinancialTransaction extends EnmaEntity {

    private String reference;
    private String description;
    private Category category;

    private Actor beneficiary;
    private Actor payer;

    private Money amount;
    private double vat;
    private Date emittedDate;
    private Date expectedDate;
    private List<Payment> payments = new ArrayList<>();
    private Status status = Status.draft;

    public enum Status {
        draft, active, canceled, partialyCanceled, payed
    }

    public static class Payment extends EnmaEntity {
        Date accountingDate; // util pour les travaux constates d'avances
        Date paymentDate;
        Money amount;
    }
}
