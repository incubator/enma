package org.chorem.enma.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class Address extends EnmaEntity {
    private String street;
    private String postalCode;
    private String city;
    private String state;
    private String country;
}
