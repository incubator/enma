package org.chorem.enma.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString(callSuper = true)
public class Task extends Period {
    private String name;
    private String description;
    private Money amount;
    private double estimatedDays;
    private String externalLink;
    private List<Task> childTasks = new ArrayList<>();

    private int progress; // % d'avancement de la tache
    private List<ConsumedTime> consumedTime = new ArrayList<>();

    public double getEstimatedDays() {
        double subdays =  childTasks.stream().mapToDouble(t -> t.getEstimatedDays()).sum();
        return estimatedDays + subdays;
    }

    public Money getAmount() {
        Money result =  childTasks.stream().map(t -> t.getAmount()).reduce(amount, (a,b) -> amount.add(b));
        return result;
    }

    public long getConsumedTime() {
        long time = consumedTime.stream().mapToLong(t -> t.getDuration()).sum();
        long childTime =  childTasks.stream().mapToLong(t -> t.getConsumedTime()).sum();

        return time;
    }
}
