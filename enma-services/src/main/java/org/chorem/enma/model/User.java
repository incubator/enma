package org.chorem.enma.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class User extends EnmaEntity {
    private String login;
    private String password;
    private Person person;
}
