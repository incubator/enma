package org.chorem.enma.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.EnumSet;
import java.util.Set;

@Getter
@Setter
@ToString(callSuper = true)
public class Company extends Actor {
    private String name;
    private String legalForm;
    private EnumSet<Relationship> relationship = EnumSet.noneOf(Relationship.class);

    public enum Relationship {
        customer, provider, other
    }
}
