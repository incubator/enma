package org.chorem.enma.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.LinkedHashMap;
import java.util.Map;

@Getter
@Setter
@ToString(callSuper = true)
public abstract class Actor extends Period {
    private Map<String, Address> addresses = new LinkedHashMap<>();
    private Map<String, Email> emails = new LinkedHashMap<>();
    private Map<String, Phone> phones = new LinkedHashMap<>();
}
