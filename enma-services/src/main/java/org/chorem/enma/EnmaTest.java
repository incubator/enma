package org.chorem.enma;

import org.chorem.enma.events.PersonCreate;
import org.chorem.enma.events.UserCreated;
import org.chorem.enma.events.UserDeleted;
import org.chorem.enma.events.UserPersonAssigned;
import org.chorem.enma.model.Person;
import org.chorem.enma.model.User;
import org.nuiton.poome.cqrs.EventStore;
import org.nuiton.poome.cqrs.Store;
import org.nuiton.poome.cqrs.persistence.PersistenceStoreFile;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Date;
import java.util.UUID;
import java.util.stream.Collectors;

public class EnmaTest {
    public static void main(String... args) throws IOException {
        Path path = FileSystems.getDefault().getPath("target", "tmp", "enma-data");
        Store store = new Store(new PersistenceStoreFile(path));

        UserCreated e = new UserCreated();
        e.setUid(UUID.randomUUID().toString());
        e.setLogin("poussin");
        e.setPassword("xxxxxxxx");
        e.setEditorDate(new Date());

        EventStore<User> esUser = store.getEventStore(User.class);
        esUser.addEvent(e);

        PersonCreate pc = new PersonCreate();
        pc.setUid(UUID.randomUUID().toString());
        pc.setTitle("M.");
        pc.setFirstName("Benjamin");
        pc.setLastName("Poussin");
        pc.setDiploma("DESS");
        pc.setBirthDate(new Date(75, 4, 2, 9,50));
        pc.setEditorUserId(e.getUid());
        pc.setEditorDate(new Date());

        EventStore<Person> esPerson = store.getEventStore(Person.class);
        esPerson.addEvent(pc);

        UserPersonAssigned upa = new UserPersonAssigned();
        upa.setUid(e.getUid());
        upa.setPersonId(pc.getUid());
        upa.setEditorUserId(e.getUid());
        upa.setEditorDate(new Date());

        esUser.addEvent(upa);

        User user = esUser.get(e.getUid());
        System.out.println("user: " + user);

        UserDeleted ud = new UserDeleted();
        ud.setUid("38303ae1-a707-43c1-afe0-f5a3ed6325f3");//e.getUid());

        esUser.addEvent(ud);

        user = esUser.get(e.getUid());
        System.out.println("user: " + user);

        System.out.println("ids: " + esUser.getIds().collect(Collectors.toList()));
    }
}
