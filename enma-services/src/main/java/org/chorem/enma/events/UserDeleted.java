package org.chorem.enma.events;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.chorem.enma.model.User;
import org.nuiton.poome.cqrs.EventErase;
import org.nuiton.poome.cqrs.Store;

@Getter
@Setter
@ToString(callSuper = true)
public class UserDeleted extends EventErase<User> {
    private String uid;

    @Override
    public String getRootId() {
        return uid;
    }

    @Override
    public User apply(Store store, User user) {
        return null;
    }
}
