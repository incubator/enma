package org.chorem.enma.events;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.chorem.enma.model.User;
import org.nuiton.poome.cqrs.Store;

@Getter
@Setter
@ToString(callSuper = true)
public class UserCreated extends EnmaEvent<User> {

    private String uid;
    private String login;
    private String password;

    @Override
    public String getRootId() {
        return uid;
    }

    @Override
    public User apply(Store store, User user) {
        User result = new User();
        result.setUid(uid);
        result.setLogin(login);
        result.setPassword(password);

        return super.apply(store, result);
    }
}
