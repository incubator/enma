package org.chorem.enma.events;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.chorem.enma.model.Person;
import org.chorem.enma.model.User;
import org.nuiton.poome.cqrs.EventStore;
import org.nuiton.poome.cqrs.Store;

@Getter
@Setter
@ToString(callSuper = true)
public class UserPersonAssigned extends EnmaEvent<User> {
    private String uid;
    private String personId;

    @Override
    public String getRootId() {
        return uid;
    }

    @Override
    public User apply(Store store, User user) {
        EventStore<Person> esPerson = store.getEventStore(Person.class);
        Person p = esPerson.get(personId);

        user.setPerson(p);

        return super.apply(store, user);
    }

}
