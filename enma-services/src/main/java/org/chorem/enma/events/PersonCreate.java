package org.chorem.enma.events;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.chorem.enma.model.Person;
import org.nuiton.poome.cqrs.Store;

import java.util.Date;

@Getter
@Setter
@ToString
public class PersonCreate extends EnmaEvent<Person> {
    private String uid;
    private String title;
    private String firstName;
    private String lastName;
    private String diploma;

    private Date birthDate;
    private Date deathDate;

    @Override
    public String getRootId() {
        return uid;
    }

    @Override
    public Person apply(Store store, Person person) {
        Person result = new Person();
        result.setUid(uid);
        result.setTitle(title);
        result.setFirstName(firstName);
        result.setLastName(lastName);
        result.setDiploma(diploma);
        result.setBeginDate(birthDate);
        result.setEndDate(deathDate);

        return super.apply(store, result);
    }

}
