package org.chorem.enma.events;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.chorem.enma.model.Editor;
import org.chorem.enma.model.EnmaEntity;
import org.chorem.enma.model.User;
import org.nuiton.poome.cqrs.Event;
import org.nuiton.poome.cqrs.EventStore;
import org.nuiton.poome.cqrs.Store;

import java.util.Date;

@Getter
@Setter
@ToString(callSuper = true)
public abstract class EnmaEvent<A extends EnmaEntity> extends Event<A> {

    private String editorUserId;
    private Date editorDate;

    @Override
    public A apply(Store store, A a) {
        EventStore<User> esUser = store.getEventStore(User.class);
        User editorUser = esUser.get(editorUserId);

        Editor editor = new Editor();
        editor.setUser(editorUser);
        editor.setDate(editorDate);

        a.getEditors().add(editor);

        return a;
    }

}
