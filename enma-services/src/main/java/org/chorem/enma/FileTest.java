package org.chorem.enma;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributeView;

public class FileTest {
    public static void main(String... args) throws IOException {
        Path src = FileSystems.getDefault().getPath("target", "tmp", "enma-data", "org.chorem.enma.model.User", "a2f9f425-a736-4787-b859-77e18cb794d2-bis");
        Path dst = FileSystems.getDefault().getPath("target", "tmp", "enma-data", "org.chorem.enma.model.User", "a2f9f425-a736-4787-b859-77e18cb794d2");
        BasicFileAttributeView att = Files.getFileAttributeView(src, BasicFileAttributeView.class);
        System.out.println(att.readAttributes().creationTime());
        Files.move(src, dst);
        att = Files.getFileAttributeView(dst, BasicFileAttributeView.class);
        System.out.println(att.readAttributes().creationTime());
    }

}
