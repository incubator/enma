package org.chorem.enma;

import com.orientechnologies.orient.core.exception.ODatabaseException;
import com.orientechnologies.orient.object.db.OObjectDatabaseTx;
import org.chorem.enma.entities.Company;

public class EnmaDB {

    protected String getUrl() {
        return "plocal:/tmp/tb/db";
    }

    /**
     * open database
     */
    public OObjectDatabaseTx open() {
        OObjectDatabaseTx db;
        // TODO: check if database exists, if not throw exception
        try {
            db = new OObjectDatabaseTx(getUrl()).create();
        } catch (ODatabaseException e){
            db = new OObjectDatabaseTx(getUrl()).open("admin","admin");

        }
        db.getEntityManager().registerEntityClass(Company.class);

        return db;
    }

    public void close(OObjectDatabaseTx db) {
        db.close();
    }

}
