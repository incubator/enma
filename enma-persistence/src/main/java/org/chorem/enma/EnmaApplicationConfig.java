package org.chorem.enma;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;

public class EnmaApplicationConfig {

    private static final Log log = LogFactory.getLog(EnmaApplicationConfig.class);

    protected ApplicationConfig applicationConfig;

    public EnmaApplicationConfig(String configFileName) {

        applicationConfig = new ApplicationConfig();

        // to allow using wao.config.path environment variable
        applicationConfig.setAppName("enma");

        applicationConfig.setEncoding("UTF-8");

        applicationConfig.setConfigFileName(configFileName);

        try {
            applicationConfig.parse();
        } catch (ArgumentsParserException e) {
            throw new EnmaTechnicalException(e);
        }

        if (log.isInfoEnabled()) {
            log.info(applicationConfig.getFlatOptions());
        }
    }

}
