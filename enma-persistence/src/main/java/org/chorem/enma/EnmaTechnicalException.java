package org.chorem.enma;

/**
 * Created by couteau on 05/12/15.
 */
public class EnmaTechnicalException  extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public EnmaTechnicalException() {
    }

    public EnmaTechnicalException(String message) {
        super(message);
    }

    public EnmaTechnicalException(String message, Throwable cause) {
        super(message, cause);
    }

    public EnmaTechnicalException(Throwable cause) {
        super(cause);
    }
}