package org.chorem.enma.entities;

/**
 * Created by couteau on 29/04/16.
 */
public class Company {

    public static String className = "Company";
    public static String nameProperty = "name";

    public String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
